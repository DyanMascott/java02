package deloitte.academy.lesson02.array;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * True or False Exercise
 * 
 * @author dmascott
 *
 */
public class TrueOrFalse {

	/*
	 * The object Logger is initialized, it will obtain the method name as a value
	 */
	private static final Logger LOGGER = Logger.getLogger(TrueOrFalse.class.getName());

	/**
	 * Method valuesTrueOrFalse
	 * 
	 * @param booleans Array with String values
	 * @return List with two inner Lists, trueVals and falseVals
	 * 
	 *         Separate the initial List into two lists depending on the value, if
	 *         the value is true, then the value is saved in the trueVals list,
	 *         otherwise the value is saved in the falseValse list
	 */
	public List[] valuesTrueOrFalse(String[] booleans) {
		List<String> trueVals = new ArrayList();
		List<String> falseVals = new ArrayList();

		try {
			for (String vals : booleans) {
				if (vals == "true") {
					trueVals.add(vals);
				} else {
					falseVals.add(vals);
				}
			}
		} catch (Exception e) {
			LOGGER.log(Level.SEVERE, "The exception is: ", e);
		}

		return new List[] { trueVals, falseVals };
	}
}
