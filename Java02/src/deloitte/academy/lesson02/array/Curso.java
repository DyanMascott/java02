package deloitte.academy.lesson02.array;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Add, Remove and Order Example
 * 
 * @author dmascott
 *
 */
public class Curso {

	/*
	 * Two variables are created, name and date
	 */
	public String name;
	public String date;

	/*
	 * The getters and setters of the variables are created
	 */
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	/*
	 * The object Logger is initialized, it will obtain the method name as a value
	 */
	private static final Logger LOGGER = Logger.getLogger(Curso.class.getName());

	/**
	 * Method addDate
	 * 
	 * @param names Array with String values
	 * @param dates Array with String values
	 * @param curso Values of the object curso
	 * @return List with two inner lists, nameList and dateList
	 * 
	 *         Add into the nameList all the values from the Array name, then the
	 *         value name from the object curso Add into the dateList all the values
	 *         from the Array date, then the value date from the object curso
	 */
	public List[] addData(String[] names, String[] dates, Curso curso) {
		List<String> nameList = new ArrayList<String>();
		List<String> dateList = new ArrayList<String>();

		try {
			for (String name : names) {
				nameList.add(name);
			}
			nameList.add(curso.getName());

			for (String date : dates) {
				dateList.add(date);
			}
			dateList.add(curso.getDate());

		} catch (Exception e) {
			LOGGER.log(Level.SEVERE, "The exception is: ", e);
		}

		return new List[] { nameList, dateList };
	}

	/**
	 * Method deleteDataByName
	 * 
	 * @param names     Array with String values
	 * @param dates     Array whit String values
	 * @param nameValue String value
	 * @return List with two inner lists, nameList and DateList
	 * 
	 *         Add into the nameList all the values from the Array name Add into the
	 *         dateList all the values from the Array date
	 * 
	 *         Search into nameList the nameValue, when the name is found the
	 *         position of the name in the list is saved. The value of the nameList
	 *         in the position saved is deleted The value of the dateList in the
	 *         position saved is deleted
	 * 
	 */
	public List[] deleteDataByName(String[] names, String[] dates, String nameValue) {
		List<String> nameList = new ArrayList<String>();
		List<String> dateList = new ArrayList<String>();

		try {
			for (String name : names) {
				nameList.add(name);
			}
			for (String date : dates) {
				dateList.add(date);
			}

			int position = 0;

			while (names[position] != nameValue) {
				position++;
			}
			nameList.remove(position);
			dateList.remove(position);

		} catch (Exception e) {
			LOGGER.log(Level.SEVERE, "The exception is: ", e);
		}

		return new List[] { nameList, dateList };
	}

	/**
	 * Method deleteDataByDate
	 * 
	 * @param names     Array with String values
	 * @param dates     Array with String values
	 * @param dateValue String value
	 * @return List with two inner lists, nameList and DateList
	 * 
	 *         Add into the nameList all the values from the Array name Add into the
	 *         dateList all the values from the Array date
	 * 
	 *         Search into dateList the dateValue, when the date is found the
	 *         position of the date in the list is saved. The value of the nameList
	 *         in the position saved is deleted The value of the dateList in the
	 *         position saved is deleted
	 */
	public List[] deleteDataByDate(String[] names, String[] dates, String dateValue) {
		List<String> nameList = new ArrayList<String>();
		List<String> dateList = new ArrayList<String>();

		try {
			for (String name : names) {
				nameList.add(name);
			}
			for (String date : dates) {
				dateList.add(date);
			}

			int position = 0;

			while (dates[position] != dateValue) {
				position++;
			}
			nameList.remove(position);
			dateList.remove(position);

		} catch (Exception e) {
			LOGGER.log(Level.SEVERE, "The exception is: ", e);
		}

		return new List[] { nameList, dateList };
	}

	/**
	 * Method orderByName
	 * 
	 * @param names Array with String values
	 * @return List with String values
	 * 
	 *         Add into the nameList all the values from the Array names
	 * 
	 *         Then sorts the values of nameList and return it
	 */
	public List<String> orderByName(String[] names) {
		List<String> nameList = new ArrayList<String>();

		try {
			for (String name : names) {
				nameList.add(name);
			}

			Collections.sort(nameList);

		} catch (Exception e) {
			LOGGER.log(Level.SEVERE, "The exception is: ", e);
		}

		return nameList;
	}

	/**
	 * Method orderByDate
	 * 
	 * @param dates Array with String values
	 * @return List with String values
	 * 
	 *         Add into the dateList all the values from the Array dates
	 * 
	 *         Then sorts the values of dateList and return it
	 */
	public List<String> orderByDate(String[] dates) {
		List<String> dateList = new ArrayList<String>();

		try {

			for (String date : dates) {
				dateList.add(date);
			}

			Collections.sort(dateList);

		} catch (Exception e) {
			LOGGER.log(Level.SEVERE, "The exception is: ", e);
		}
		return dateList;
	}

}
