package deloitte.academy.lesson02.array;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Serching Word Exercise
 * 
 * @author dmascott
 *
 */
public class Word {

	/*
	 * The object Logger is initialized, it will obtain the method name as a value
	 */
	private static final Logger LOGGER = Logger.getLogger(Word.class.getName());

	/**
	 * Method searchWord
	 * 
	 * @param words        Array with String values
	 * @param wordToSearch String value
	 * @return Int with position where the word is located
	 * 
	 *         Compares the wordToSearch with every other word in the words list,
	 *         when the value is found in the list, the position of the word is
	 *         taken and saved in the position variable, and then returned to the
	 *         main class
	 */
	public int searchWord(String[] words, String wordToSearch) {
		int position = 0;

		try {
			while (words[position] != wordToSearch) {
				position = position + 1;
			}
			position = position + 1;

		} catch (Exception e) {
			LOGGER.log(Level.SEVERE, "The exception is: ", e);
		}

		return position;
	}
}
