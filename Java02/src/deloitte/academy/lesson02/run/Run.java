package deloitte.academy.lesson02.run;

import java.util.List;
import java.util.logging.Logger;

import deloitte.academy.lesson02.array.*;

/**
 * Main class of the project Java02
 * 
 * @author dmascott
 *
 */
public class Run {

	/*
	 * The object Logger is initialized, it will obtain the method name as a value
	 */
	private static final Logger LOGGER = Logger.getLogger(Run.class.getName());

	/**
	 * Main method
	 * 
	 * @param args
	 */
	public static void main(String[] args) {

		/*
		 * Instantiates the class BiggerSmallerRepeated
		 */
		BiggerSmallerRepeated bigSmallRepeat = new BiggerSmallerRepeated();

		/*
		 * Instantiates the class TrueOrFalse
		 */
		TrueOrFalse trueOrFalse = new TrueOrFalse();

		/*
		 * Instantiates the class Word
		 */
		Word word = new Word();

		/*
		 * Instantiates the class Curso
		 */
		Curso curso = new Curso();

		/*
		 * An Array with int values is created
		 */
		int[] marks = { 125, 132, 95, 116, 120, 132 };

		/*
		 * The method maxMin from the class BiggerSmallerRepeated is called
		 */
		List<Integer> maxMin = bigSmallRepeat.maxMin(marks);
		LOGGER.info("The highest score is: " + maxMin.get(0) + "\n");
		LOGGER.info("The lowest score is: " + maxMin.get(1) + "\n");

		/*
		 * The method repeated from the class BiggerSmallerRepeated is called
		 */
		List<Integer> rep = bigSmallRepeat.repeated(marks);
		LOGGER.info("The repeated numbers are: " + rep + "\n");

		/*
		 * An Array with String values of true, false is created
		 */
		String[] booleanValues = { "true", "false", "true", "true" };

		/*
		 * The method valuesTrueOrFalse from the class TrueOrFalse is called
		 */
		List[] vals = trueOrFalse.valuesTrueOrFalse(booleanValues);
		LOGGER.info("True values: " + vals[0] + "\n");
		LOGGER.info("False values: " + vals[1] + "\n");

		/*
		 * An Array with String values is created
		 */
		String[] words = { "Hello", "there", "Java", "Academy" };

		/*
		 * The method searchWord from the class Word is called
		 */
		int positionWord = word.searchWord(words, "Java");
		LOGGER.info("The word Java is in the position: " + positionWord + "\n");

		/*
		 * Two Arrays with String values are created The first Array has names and the
		 * second Array has dates
		 */
		String[] names = { "Dyan", "Kevyn", "Brenda", "Jazz" };
		String[] dates = { "11/29", "01/22", "07/02", "08/18" };

		/*
		 * A new name value is setted to the object curso A new date value is setted to
		 * the object curso
		 */
		curso.setName("Karina");
		curso.setDate("05/25");

		/*
		 * The method addData from the class Curso is called
		 */
		List[] add = curso.addData(names, dates, curso);
		LOGGER.info("List with new names: " + add[0] + "\n");
		LOGGER.info("List with new dates: " + add[1] + "\n");

		/*
		 * The method deleteDataByName from the class Curso is called
		 */
		List[] removeByName = curso.deleteDataByName(names, dates, "Dyan");
		LOGGER.info("Name list with deleted name: " + removeByName[0] + "\n");
		LOGGER.info("Date list with deleted name: " + removeByName[1] + "\n");

		/*
		 * The method deleteDataByDate from the class Curso is called
		 */
		List[] removeByDate = curso.deleteDataByDate(names, dates, "01/22");
		LOGGER.info("Name list with deleted date: " + removeByDate[0] + "\n");
		LOGGER.info("Date list with deleted date: " + removeByDate[1] + "\n");

		/*
		 * The method orderByName from the class Curso is called
		 */
		List<String> orderByName = curso.orderByName(names);
		LOGGER.info("Ordered names:  " + orderByName + "\n");

		/*
		 * The method orderByDate from the class Curso is called
		 */
		List<String> orderByDate = curso.orderByDate(dates);
		LOGGER.info("Ordered dates: " + orderByDate + "\n");
	}

}
