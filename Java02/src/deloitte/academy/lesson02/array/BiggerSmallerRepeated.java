package deloitte.academy.lesson02.array;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Bigger, Smaller and Repeated Numbers Exercise
 * 
 * @author dmascott
 *
 */
public class BiggerSmallerRepeated {

	/*
	 * The object Logger is initialized, it will obtain the method name as a value
	 */
	private static final Logger LOGGER = Logger.getLogger(BiggerSmallerRepeated.class.getName());

	/**
	 * Method maxMin
	 * 
	 * @param numbers Array with int values
	 * @return values List with int values, the two values returned are min and max
	 * 
	 *         Compares each number of the list with the rest of the list numbers to
	 *         obtain the bigger and smaller number
	 */
	public List<Integer> maxMin(int[] numbers) {
		List<Integer> values = new ArrayList<Integer>();

		int min = numbers[0];
		int max = numbers[0];

		try {
			for (int num : numbers) {
				if (num > max) {
					max = num;
				}
			}
			for (int num : numbers) {
				if (num < min) {
					min = num;
				}
			}

			values.add(max);
			values.add(min);

		} catch (Exception e) {
			LOGGER.log(Level.SEVERE, "The exception is: ", e);
		}
		return values;
	}

	/**
	 * Method repeated
	 * 
	 * @param numbers Array with int values
	 * @return values List with int values, the values returned are the repeated
	 *         numbers in the list
	 * 
	 *         Compares each number of the list with the rest of the list numbers to
	 *         obtain the repeated numbers
	 */
	public List<Integer> repeated(int[] numbers) {
		List<Integer> values = new ArrayList<Integer>();

		int rep = numbers[0];
		int length = numbers.length;

		try {
			int quantity = 1;
			for (int num : numbers) {
				for (int i = quantity; i < length; i++) {
					if (num == numbers[i]) {
						rep = num;
						if (!values.contains(rep)) {
							values.add(rep);
						}
					}
				}
				quantity = quantity + 1;
			}
		} catch (Exception e) {
			LOGGER.log(Level.SEVERE, "The exception is: ", e);
		}
		return values;
	}

}
